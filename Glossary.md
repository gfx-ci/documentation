# Glossary

## <a name="Orchestrator"></a>Orchestrator

Single common server that announces changes and recives reports from
[CI Farms](#CIFarm) and [builders](#Builder).

## <a name="CIFarm"></a>CI Farm

A set of machines ([DUT](#DUT)s) along with a single controller, which
receives jobs from the [orchestrator](#Orchestrator) and selectively executes
them on the DUTs. A CI Farm is dedicated to HW testing.

## <a name="DUT"></a>DUT

Device Under Test, a hardware platform located in a [CI Farm](#CIFarm) that
execute a set of tests, as asked by the [CI Farm](#CIFarm)'s controller.

## <a name="Builder"></a>Builder

One or more builds bots, which receives jobs from the
[orchestrator](#Orchestrator).
