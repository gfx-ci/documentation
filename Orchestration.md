# Orchestration

## Reasoning

There is no way we can build a single testing lab that covers all hardware that
all graphics developers are interested in. Yet we don't want developers to have
to look at multiple CI Farms for status or submit work to.

Which is why a single central orchestration point that generates changeset from
multiple repos that are then announced and provides reports from testing in CI
Farms or builders that pick up the announced changesets.

What the Orchestrator should not do is run jobs on specific CI Farms or DUTs.
The controller of a CI Farm will consume the stream of changesets and act upon
CI Farm specific rules, like trusts of the developer or which repo it is
interested in.

Part of the design should be that anybody can subscribe, follow or explore the
announced changesets. This allows for a way of entry for new CI Farms that does
not require admin intervention. At least until the CI Farm has been deemed
stable enough to allow uploading of results or impact if a changset should get
a green or red status. Trustability of the changset results are paramount for
developers, as if they can't trust the results they will not use it. So the
results given to the Orchestrator from a CI Farm must be stable.

## Requirements

  * The orchestration should be changeset-centric.
  * A changeset may cover changes accross multiple repos.
  * Provide chain of trust and authentication information to changeset.
  * Support multiple-frontends to get trees for changesets, like gitlab or
    patchwork. Repos and patches would need to be in the open.
  * Allow trusted CI Farms and builders to upload results.
  * Enable anybody to subscribe and explore changesets to build, with read only
    access.

## Non-requirements

  * Dispatching jobs to CI Farms, builders or DUTs.
